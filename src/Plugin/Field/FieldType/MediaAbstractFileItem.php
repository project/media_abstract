<?php

namespace Drupal\media_abstract\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\file\Plugin\Field\FieldType\FileItem;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;

/**
 * Plugin implementation of the 'media_abstract_file' field type.
 *
 * @FieldType(
 *   id = "media_abstract_file",
 *   label = @Translation("Media abstract file"),
 *   description = @Translation("This field stores the ID of a file as an integer value."),
 *   category = "reference",
 *   default_widget = "media_abstract_file",
 *   default_formatter = "media_abstract_file",
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 */
class MediaAbstractFileItem extends FileItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    return [
      'media_types' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $element = parent::fieldSettingsForm($form, $form_state);

    // Allowed file extensions derive from the allowed media types.
    unset($element['file_extensions']);

    $element['media_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Media types'),
      '#description' => $this->t('Enable one or more media types. Allowed extensions are automatically inherited from the enabled media types.'),
      '#options' => [],
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#default_value' => $this->getSetting('media_types'),
    ];
    /** @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo */
    $entityTypeBundleInfo = \Drupal::service('entity_type.bundle.info');
    foreach ($entityTypeBundleInfo->getBundleInfo('media') as $bundle => $definition) {
      $element['media_types']['#options'][$bundle] = $definition['label'];
    }

    return $element;
  }

  /**
   * Get the allowed media types.
   *
   * @return array
   *   Returns an array of media types source fields.
   */
  public function getMediaTypes(): array {
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $items = [];

    foreach ($this->getSetting('media_types') as $bundle) {
      /** @var \Drupal\media\MediaTypeInterface $mediaType */
      if ($mediaType = \Drupal::entityTypeManager()->getStorage('media_type')->load($bundle)) {
        $fieldDefinitions = $entityFieldManager->getFieldDefinitions('media', $mediaType->id());
        $items[$mediaType->id()] = $fieldDefinitions[$mediaType->get('source_configuration')['source_field']];
      }
    }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getUploadValidators(): array {
    $validators = parent::getUploadValidators();

    // Gather allowed file extensions from media types.
    $extensions = [];
    foreach ($this->getMediaTypes() as $field) {
      /** @var \Drupal\field\Entity\FieldConfig $field */
      $extensions[] = $field->getSetting('file_extensions');
    }
    $extensions = implode(' ', $extensions);

    if (isset($validators['file_validate_extensions'])) {
      $validators['file_validate_extensions'] = [$extensions];
    }
    else {
      $validators['FileExtension'] = ['extensions' => $extensions];
    }

    return $validators;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    // Create or update the files media entity.
    $value = $this->getValue();
    if ($media = $this->loadMedia(TRUE)) {
      $value['mid'] = $media->id();
    }
    elseif ($media = $this->createMedia()) {
      $value['mid'] = $media->id();
    }
    $this->setValue($value);
  }

  /**
   * Load the referenced files media entity.
   *
   * @param bool $sync
   *   Boolean indicating to sync language with parent entity.
   *
   * @return \Drupal\media\MediaInterface|null
   *   Returns the referenced files media entity. Otherwise, NULL.
   */
  public function loadMedia(bool $sync = FALSE): ?MediaInterface {
    if (($value = $this->getValue()) && isset($value['mid']) && $value['mid']) {
      if ($media = \Drupal::entityTypeManager()->getStorage('media')->load($value['mid'])) {
        /** @var \Drupal\media\MediaInterface $media */
        if ($sync && $media->language()->getId() !== ($langcode = $this->getEntity()->language()->getId())) {
          $media->set('langcode', $langcode);
          $media->save();
        }
        return $media;
      }
    }
    return NULL;
  }

  /**
   * Create the referenced files media entity.
   *
   * @return \Drupal\media\MediaInterface|null
   *   Returns the referenced files media entity. Otherwise, NULL.
   */
  public function createMedia(): ?MediaInterface {
    /** @var \Drupal\Core\Entity\Plugin\DataType\EntityReference $entity */
    if ($entity = $this->get('entity')) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $entity->getTarget()->getValue();

      foreach ($this->getMediaTypes() as $bundle => $field) {
        /** @var \Drupal\field\Entity\FieldConfig $field */
        $extensions = preg_replace('/ +/', '|', preg_quote($field->getSetting('file_extensions') ?: ""));
        if (preg_match("/\\.($extensions)$/i", $file->getFilename())) {
          $entity = Media::create([
            'bundle' => $bundle,
            'uid' => $file->getOwnerId(),
            'langcode' => $this->getEntity()->language()->getId(),
            $field->getName() => $file->id(),
            'changed' => $file->getChangedTime(),
            'created' => $file->getCreatedTime(),
          ]);
          $entity->save();
          return $entity;
        }
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $schema = parent::schema($field_definition);
    $schema['columns']['mid'] = [
      'description' => 'The media ID of the target file.',
      'type' => 'int',
      'unsigned' => TRUE,
    ];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = parent::propertyDefinitions($field_definition);
    $properties['mid'] = DataDefinition::create('string')
      ->setLabel(t('Media ID'))
      ->setDescription(t('The media ID of the target file.'));
    return $properties;
  }

}
