<?php

namespace Drupal\media_abstract\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'media_abstract_file' formatter.
 *
 * @FieldFormatter(
 *   id = "media_abstract_file",
 *   label = @Translation("Rendered entity"),
 *   field_types = {
 *     "media_abstract_file"
 *   }
 * )
 */
class MediaAbstractFileFormatter extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);
    $form['view_mode']['#options'] = ['default' => $this->t('Default')];
    foreach ($this->entityDisplayRepository->getViewModes('media') as $viewMode => $definition) {
      $form['view_mode']['#options'][$viewMode] = $definition['label'];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $build = [];
    foreach ($items as $item) {
      /** @var \Drupal\media_abstract\Plugin\Field\FieldType\MediaAbstractFileItem $item */
      if ($media = $item->loadMedia()) {
        $build[] = $this->entityTypeManager->getViewBuilder('media')->view($media, $this->getSetting('view_mode'));
      }
    }
    return $build;
  }

}
