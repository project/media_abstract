<?php

namespace Drupal\media_abstract\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides media entity route subscriber.
 */
class MediaRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $routes = [
      'entity.media.add_page',
      'entity.media.add_form',
    ];
    foreach ($routes as $routeName) {
      if ($route = $collection->get($routeName)) {
        $route->setRequirement('_permission', 'access media add routes');
      }
    }
  }

}
