# Media Abstract

Media types are a great way to organize and improve the rendering of various file types. Unfortunately, media alone
is unable to provide a single file input for multiple media types. This module provides a new "Media abstract file"
field type and formatter. It appears and acts like a standard file field. On save, the magic begins, each uploaded
file automatically creates a media entity matching the file extension. Each field may enable specific media types.

## Usage

1. Download and install the `drupal/media_abstract` module. Recommended install method is composer:
   ```
   composer require drupal/media_abstract
   ```
2. Go to the "Manage fields" page of the desired entity type.
3. Create a new "Media abstract file" field.
4. Review the available configurations and save the instance.
5. Place the field in the desired view and form modes.

## Caveats

1. When two media types are enabled and have overlapping allowed extensions, the first media type matching the file
   upload extension is used. Typically, media types have unique allowed extensions so this support isn't in
   high demand. Please create a ticket if you have a suggestion on how to support overlapping extensions.

2. On edit the user may remove uploaded files. Only the file entity reference is removed, the media entity and original
   file remain intact. This is by design but feel free to open a ticket if you have an alternative solution. Media
   entities created from uploaded files may be modified or removed as usual.

## Authoring

Automatically created media entities are owned by the current user. Regardless of the parent entity owner.

## Language and translations

New files on translatable fields create additional media entities of the same language. In other words, translated
file uploads are never saved as translations of existing media entities. If the source language of the parent
entity changes, source language also changes on the referenced media entities.

## Bonus!

Use the "Access media add pages" permission to control access to the media add and create routes.
