<?php

namespace Drupal\media_abstract\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Plugin implementation of the 'media_abstract_file' widget.
 *
 * @FieldWidget(
 *   id = "media_abstract_file",
 *   label = @Translation("File"),
 *   field_types = {
 *     "media_abstract_file"
 *   }
 * )
 */
class MediaAbstractFileWidget extends FileWidget {

  /**
   * {@inheritdoc}
   */
  public static function process($element, FormStateInterface $form_state, $form) {
    $element = parent::process($element, $form_state, $form);
    $element['mid'] = [
      '#type' => 'hidden',
      '#value' => $element['#value']['mid'] ?? NULL,
    ];
    return $element;
  }

}
